package roboSoc.Controller;

import roboSoc.Core.IObservador;
import roboSoc.Core.Partido;

public class CtrlPrincipal implements IObservador {

	IFrmPrincipal _vista;
	Partido _partido;
	
	public CtrlPrincipal(IFrmPrincipal vista, Partido partido)
	{
		this._vista=vista;
		this._partido=partido;
		
		this._partido.agregarObservador(this);
	}
	
	
	
	@Override
	public void actualizar() {
		// TODO Auto-generated method stub
		this._vista.actualizarVista();
	}

}
