package roboSoc.Controller;

import static org.junit.Assert.*;

import org.junit.Test;

import roboSoc.Core.Partido;

public class CtrlPrincipalTEST {

	class MockPartido extends Partido{
		
		public MockPartido()
		{
			super();
			
		}
	};	
	
	class MockVista implements IFrmPrincipal{
		
		private boolean _actualizada=false;
		
		public MockVista()
		{
			super();
			
		}

		@Override
		public void actualizarVista() {
			this._actualizada=true;
		}
		
		public boolean fueActualizada() {
			return this._actualizada;
		}
	}	
	
	MockVista _vista;
	
	
	@Test
	public void testCtrlPrincipal() {
		
		CtrlPrincipal test=new CtrlPrincipal(new MockVista(), new MockPartido()); 
		
		assertNotNull(test);
	}

	@Test
	public void testActualizar() {
		this._vista=new MockVista();
		
		CtrlPrincipal test=new CtrlPrincipal(_vista, new MockPartido()); 
		test.actualizar();
		assertTrue(_vista.fueActualizada());
		
		assertTrue(true);
	}

}
